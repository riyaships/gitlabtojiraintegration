namespace Awsm.Tools.Web.Models
{
    public class IssuesViewModel
    {
        public string PrivateToken { get; set; }
        public string UserName { get; set; }
        public int ProjectId { get; set; }
    }
}