﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models;
using Awsm.Tools.Data.Models.WebConfig;
using Awsm.Tools.HttpServices.Interfaces;
using Awsm.Tools.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Awsm.Tools.Web.Models;
using Microsoft.AspNetCore.Authorization;

namespace Awsm.Tools.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IGitLabOAuthService _oAuthService;
        private readonly IGitlabApiService _gitApiService;
        public IActionResult Index()
        {
            return View();
        }
        public HomeController(IGitLabOAuthService oAuthService, IGitlabApiService gitApiService)
        {
            _oAuthService = oAuthService;
            _gitApiService = gitApiService;
        }

        [HttpPost]
        [Route("projects/list")]
        public async Task<ActionResult> Projects(IssuesViewModel model)
        {
            var result = await _gitApiService.GetUserProjectsAsync(model.UserName, model.PrivateToken);
            ViewData["PrivateToken"] = model.PrivateToken;
            return View(result);
        }

        [HttpPost]
        [Route("issues/list")]
        public async Task<ActionResult> Issues(IssuesViewModel model)
        {
            var result = await _gitApiService.GetUserProjectIssuesAsync(model.UserName, model.PrivateToken, model.ProjectId);
            ViewData["PrivateToken"] = model.PrivateToken;
            ViewData["ProjectId"] = model.ProjectId;
            return PartialView("Partials/_Issues", result);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
