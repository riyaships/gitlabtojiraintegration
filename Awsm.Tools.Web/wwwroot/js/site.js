﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var JsHelpers = {};
(function (jQuery, jsHelpers) {

    jsHelpers.AjaxService = function () {

        var getFormObject = function(formId) {
            var dataArray = $("#" + formId).serializeArray(), dataObj = {};
            $(dataArray).each(function (i, field) {
                dataObj[field.name] = field.value;
            });
            return dataObj;
        }

        var performAjaxPost = function (actionUrl, data, successCallBack, contentType) {
            var request = $.ajax({
                url: actionUrl,
                type: "POST",
                cache: false,
                data: data,
                contentType: contentType,
                success: function(response) {
                    if (successCallBack != null) {
                        successCallBack(response);
                    }
                    return response;
                },
                error: function(data) {

                },
                fail: function(data) {
                    window.location.href = "/account/login";
                }
            });
            return request.responseText;
        };

        return {
            PerformAjaxPost: function(actionUrl, data, successCallBack, contentType) {
                return performAjaxPost(actionUrl, data, successCallBack, contentType);
            },
            GetFormObject: function (formId) {
                return getFormObject(formId);
            }
        };
    }
    jsHelpers.AjaxService = new jsHelpers.AjaxService();
    }) (jQuery, JsHelpers);