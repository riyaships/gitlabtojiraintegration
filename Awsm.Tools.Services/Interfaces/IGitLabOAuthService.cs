﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Awsm.Tools.Services.Interfaces
{
    public interface IGitLabOAuthService
    {
        Task<HttpResponseMessage> LoginAsync();
    }
}
