﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models;

namespace Awsm.Tools.Services.Interfaces
{
    public interface IGitlabApiService
    {
        Task<IEnumerable<GitlabProject>> GetUserProjectsAsync(string username, string privateToken);
        Task<IEnumerable<GitlabIssue>> GetUserProjectIssuesAsync(string username, string privateToken, int projectId);
    }
}