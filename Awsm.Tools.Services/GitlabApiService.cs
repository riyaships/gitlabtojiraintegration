﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models;
using Awsm.Tools.Data.Models.WebConfig;
using Awsm.Tools.HttpServices.Interfaces;
using Awsm.Tools.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace Awsm.Tools.Services
{
    
    public class GitlabApiService : IGitlabApiService
    {
        private readonly IAwsmHttpService _httpService;
        private readonly AppConfigValues _config;
        public GitlabApiService(IAwsmHttpService httpService, IOptions<AppConfigValues> config)
        {
            _httpService = httpService;
            _config = config.Value;
        }

        public async Task<IEnumerable<GitlabProject>> GetUserProjectsAsync(string username, string privateToken)
        {
            var endpoint = _config.ProjectsEndpoint.Replace("{username}", username);
            var result = await _httpService.GetAsync<List<GitlabProject>>(_config.GitlabBaseAddress, endpoint, GetApiHeaders(privateToken));
            return result;
        }

        public async Task<IEnumerable<GitlabIssue>> GetUserProjectIssuesAsync(string username, string privateToken, int projectId)
        {
            var endpoint = _config.IssuesEndpoint.Replace("{username}", username);
            var allIssues = await _httpService.GetAsync<List<GitlabIssue>>(_config.GitlabBaseAddress, endpoint, GetApiHeaders(privateToken));
            var result = allIssues.Where(m => m.ProjectId == projectId).ToList();
            await LoadIssueNotes(result, privateToken);
            return result;
        }

        private async Task LoadIssueNotes(List<GitlabIssue> issues, string privateToken)
        {
            foreach (var issue in issues)
            {
                var urlPath = issue.Links.Notes.Replace(_config.GitlabBaseAddress, string.Empty);
                issue.IssueNotes = await _httpService.GetAsync<List<GitlabIssueNote>>(_config.GitlabBaseAddress, urlPath, GetApiHeaders(privateToken));
            }
        }

        private Dictionary<string, string> GetApiHeaders(string privateToken)
        {
            var headers = new Dictionary<string, string>() { { "PRIVATE-TOKEN", privateToken } };
            return headers;
        }
    }
}
