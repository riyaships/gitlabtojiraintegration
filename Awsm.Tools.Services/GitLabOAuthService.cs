﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models.WebConfig;
using Awsm.Tools.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace Awsm.Tools.Services
{
    public class GitLabOAuthService : IGitLabOAuthService
    {
        private readonly AppConfigValues _options;

        public GitLabOAuthService(IOptions<AppConfigValues> options)
        {
            _options = options.Value;
        }
        public async Task<HttpResponseMessage> LoginAsync()
        {
            using var client = new HttpClient();

            client.BaseAddress = new Uri(_options.GitlabBaseAddress);
            client.DefaultRequestHeaders.Add("client_id", _options.GoogleClientId);
            client.DefaultRequestHeaders.Add("client_secret",_options.GoogleClientSecret);
            client.DefaultRequestHeaders.Add("grant_type", "authorization_code");
            return await client.PostAsync("/oauth/token", null);
        }
    }
}
