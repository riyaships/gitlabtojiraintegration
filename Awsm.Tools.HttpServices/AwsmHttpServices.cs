﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Awsm.Tools.HttpServices.Interfaces;
namespace Awsm.Tools.HttpServices
{
    public class AwsmHttpServices : IAwsmHttpService
    {
        public async Task<T> GetAsync<T>(string baseUrl, string uriPathWithQueryParams, Dictionary<string, string> headers=null)
        {
            var client = new HttpClient(){  BaseAddress = new Uri(baseUrl) };
            try
            {
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }
                }

                using var response = await client.GetAsync(uriPathWithQueryParams);
                using var content = response.Content;
                var d = await content.ReadAsStringAsync();
                if (d != null)
                {
                    var data = JsonConvert.DeserializeObject<T>(d);
                    return (T)data;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            Object o = new Object();
            return (T)o;
        }
         
    }
}
