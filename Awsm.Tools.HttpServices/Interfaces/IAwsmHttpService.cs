﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Awsm.Tools.HttpServices.Interfaces
{
    public interface IAwsmHttpService
    {
        Task<T> GetAsync<T>(string baseUrl, string uriPathWithQueryParams, Dictionary<string, string> headers = null);
    }
}