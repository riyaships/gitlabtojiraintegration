using Awsm.Tools.Data.Models.WebConfig;
using Awsm.Tools.Services;
using Awsm.Tools.Services.Interfaces;
using FakeItEasy;
using Microsoft.Extensions.Options;

namespace Awsm.Tools.Web.UnitTests
{
    public class BaseTestHelpers
    {
        protected IOptions<AppConfigValues> ConfigOptions;
        protected IGitLabOAuthService GitLabOAuthService;
        public BaseTestHelpers()
        {
            ConfigOptions = A.Fake<IOptions<AppConfigValues>>();
            GitLabOAuthService = new GitLabOAuthService(ConfigOptions);
        }
    }
}