using System.Net;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models.WebConfig;
using Awsm.Tools.Services;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Awsm.Tools.Web.UnitTests
{
    [TestClass]
    public class GibLabOAuthServiceIntegrationTests : BaseTestHelpers
    {
        [Ignore]
        [TestMethod]
        public async Task GivenOAuthAppIdAndSecretWhenFetchingTokenReturnsResponseSuccessfully()
        {
            //Arrange
            var settings = new AppConfigValues()
            {
                PrivateToken = "A8Fsx7tz1tZeiz6h-BAu",
                GoogleClientSecret = "4v2s3sgLJ-PNZJPZTDsd0P1_",
                GitlabBaseAddress = "https://gitlab.com"
            };
            A.CallTo(() => ConfigOptions.Value).Returns(settings);

            //Act
            GitLabOAuthService = new GitLabOAuthService(ConfigOptions);
            var response = await GitLabOAuthService.LoginAsync();

            //Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Accepted);
        }
    }
}
