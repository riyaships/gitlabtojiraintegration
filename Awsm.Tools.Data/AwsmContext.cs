﻿using System.Collections.Generic;
using System.Text;
using Awsm.Tools.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Awsm.Tools.Data
{
    public interface IAwsmContext
    {
        //DbSet<GitlabIssue> GitlabIssues { get; set; }
        //DbSet<GitlabIssueNote> IssueNotes { get; set; }
         
    }

    public class AwsmContext : IdentityDbContext, IAwsmContext
    {
        public AwsmContext(DbContextOptions<AwsmContext> options)
            : base(options)
        {
        }

        //public virtual DbSet<GitlabIssue> GitlabIssues { get; set; }
        //public virtual DbSet<GitlabIssueNote> IssueNotes { get; set; }
 
    }
}