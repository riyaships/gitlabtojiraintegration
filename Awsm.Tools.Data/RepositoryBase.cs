﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Awsm.Tools.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Awsm.Tools.Data
{
    public interface IBaseRepo<T>
    {
        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
     
    public abstract class BaseRepo<T> : IBaseRepo<T> where T : class
    {
        protected AwsmContext Context { get; set; }
        protected BaseRepo(AwsmContext cxt)
        {
            Context = cxt;
        }

        public void Create(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            Context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
        public IQueryable<T> FindAll()
        {
            return Context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> filter)
        {
            return Context.Set<T>().Where(filter).AsNoTracking();
        }

    }
}