﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Awsm.Tools.Data.Models
{
    public abstract class BaseEntity
    {
        protected BaseEntity()
        {
            RecordCreated = DateTime.UtcNow;
        }
        public DateTime RecordCreated { get; set; }
    }
}
