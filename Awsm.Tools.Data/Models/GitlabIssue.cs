﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;

namespace Awsm.Tools.Data.Models
{
    public class GitlabProject
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
    public class GitlabIssue : BaseEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("project_id")]
        public int ProjectId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("state")]
        public string Status { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        public string MilestoneTitle => Milestone.Name;
        public DateTime? MilestoneDueDate => Milestone.DateDue;
        
        [JsonProperty("labels")]
        public string[] Labels { get; set; }
        public string LabelsText => string.Join(",", Labels);

        [JsonProperty("created_at")]
        public DateTime DateCreated { get; set; }

        [JsonProperty("closed_at")]
        public DateTime? DateClosed { get; set; }

        [JsonProperty("updated_at")]
        public DateTime? DateUpdated { get; set; }

        [JsonProperty("due_date")]
        public DateTime? DateDue { get; set; }


        [JsonProperty("author")]
        public GitlabAuthor Author { get; set; }

        [JsonProperty("milestone")]
        public GitlabMilestone Milestone { get; set; }

        public string Assigned {
            get { return (Assignees != null && Assignees.Length > 0) ? string.Join(",", Assignees.Select(m=> m.Name)) : "Unassigned"; }
        }
        [JsonProperty("assignees")]
        public GitlabAssignee[] Assignees { get; set; }
        
        [JsonProperty("_links")]
        public GitlabIssueLinks Links { get; set; }

        public virtual ICollection<GitlabIssueNote> IssueNotes { get; set; }

        public string AuthorName => Author?.Name;

    }

    public class GitlabIssueLinks
    {
        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("project")]
        public string Project { get; set; }
    }

    public class GitlabAuthor
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

    }

    public class GitlabAssignee
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("web_url")]
        public string WebUrl { get; set; }

    }

    public class GitlabMilestone
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Name { get; set; }

        [JsonProperty("start_date")]
        public DateTime? StartDue { get; set; }

        [JsonProperty("due_date")]
        public DateTime? DateDue { get; set; }
    }
}