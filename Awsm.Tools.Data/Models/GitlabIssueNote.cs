﻿using System;
using Newtonsoft.Json;

namespace Awsm.Tools.Data.Models
{
    public class GitlabIssueNote : BaseEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("body")]
        public string NoteContent { get; set; }
        [JsonProperty("author.name")]
        public string AuthorName => Author?.Name;

        [JsonProperty("created_at")]
        public DateTime DateCreated { get; set; }
        [JsonProperty("updated_at")]
        public DateTime DateUpdated { get; set; }

        [JsonProperty("author")]
        public GitlabAuthor Author { get; set; }
    }
}