﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Awsm.Tools.Data.Models.WebConfig
{
    public class AppConfigValues
    {
        public string GitlabBaseAddress { get; set; }
        public string PrivateToken { get; set; }
        public string ProjectsEndpoint { get; set; }
        public string IssuesEndpoint { get; set; }
        public string GoogleClientId { get; set; }
        public string GoogleClientSecret { get; set; }
    }
}
